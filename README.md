# Projeto de um Telescópio Robótico

Estamos montando um telescópio de baixo custo, porém, com alta clareza de imágem.
É um telescópio refletor newtoniano.

So far, compramos um espelho pronto no Aliexpress, de 180mm de diâmetro e distância focal de 1250mm.
Instalamos o espelho em um cano de esgoto de 200mm de largura e 1350cm de comprimento. Usamos oculares de 10mm e de 25mm e uma Barlow de 2 vezes.

- Com a ocular de 25mm é possível ver a lua cobrindo quase que totalmente o campo de visão = aumento de 50 vezes = distância focal de 1250mm/25.
- Com a ocular de 10mm é possível ver detalhes das crateras da Lua e luas de Júpiter e Saturno = aumento de 125 vezes = distância focal de 1250mm/10.
- Com a Barlow e a ocular de 10mm conseguimos aumentos de 250 vezes (o máximo aumento recomendável é o Diametro da Objetiva x 2,5  = 180 x 2,5 = 450 vezes) que é muito bom para observar detalhes da Lua, mas sem motorização fica muito difícil seguir planetas como Júpiter e Saturno. 

<div align="center">
 <img src="./Imagens/tele1.jpg" height="300">
  <img src="./Imagens/tele2.jpg" width="300">
</div>
<div align="center">
 <img src="./Imagens/tele3.jpg" width="300">
 <img src="./Imagens/tele4.jpg" width="300">
 </div>
 <div align="center">
 <img src="./Imagens/tele5.jpg" width="300">
 <img src="./Imagens/tele6.jpg" width="300">
</div>

Usamos uma impressora 3D para imprimir várias peças para o acoplamento das oculares e uma câmera fotog´rafica Canon Rebel EOS 6s, direto na câmera sem nenhuma lente. 

## Videos Explicando como funciona um Telescópio
### Nesses vídeos estou fazendo confusão e chamando a lente ocular de objetiva. 

- Focus in Photography explained... Is it correct?? - https://youtu.be/X0Ne27aGgBQ

- Distância do plano focal em função da distância do objeto: será que está certo?? - https://youtu.be/A7B7FLVFHI0

- Papel da objetiva para enrretar os raios luminosos para o olho: será que está certo?? - https://youtu.be/NIHwdwdsPOA

- Zoom diferentes com objetivas de distância focal diferentes: será que está certo?? - https://youtu.be/8vHMTAXDtCI


## Links interessantes

* Dois sites legais de bringar: Esse funciona como um Google Earth na Lua - Eu uso para procurar os nomes das crateras e os mares que encontro com o Telescópio e procurar onde as Apolos pousaram -  https://www.google.com/maps/space/moon/@6.9087512,8.4411702,8111933a,35y,90h/data=!3m1!1e3 

* Para encontrar onde as Apolo pousaram - https://www.google.com/moon/

* Vários vídeos comentados da superfície da Lua em alta resolução (4K) obtidos com o LROC - The Lunar Reconnaissance Orbiter Camera - https://www.youtube.com/playlist?list=PL2gLpWRK0QlBOzmGtXt1y_gnS2WShTbTa

* Quickmap - Site para brincar com as imagens coletadas da NASA - https://quickmap.lroc.asu.edu/

* Imagens muito legais em alta resolução do LROC - The Lunar Reconnaissance Orbiter Camera-  http://lroc.sese.asu.edu/posts

* Seu primeiro telescópio - Fundação Planetário da Cidade do Rio de Janeiro - Informações básicas sobre modelos de telescópios - http://planeta.rio/seu-primeiro-telescopio/
