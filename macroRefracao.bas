REM  *****  BASIC  *****

Sub Main
rem inputbox "","", ThisComponent.CurrentSelection.getFormula()
End Sub

Function INCIDE_LENTE_X(tetaLuz As Double, centroX As Double, centroY As Double, objetoX As Double, objetoY As Double, lenteRaio As Double, BYVAL lentePrincipal As Boolean) As Double
	Dim mLuz As Double
	Dim a As Double
	Dim b As Double
	Dim c As Double
	Dim delta As Double
	Dim oFunctionAccess As Object
	Dim x1 As Double
	Dim x2 As Double

	oFunctionAccess = createUnoService( "com.sun.star.sheet.FunctionAccess" )

	mLuz = TAN(tetaLuz)
	a = 1 + mLuz^2
	b = 2 * (mLuz * (-mLuz * objetoX + objetoY - centroY) - centroX)
	c = objetoY * (objetoY - 2 * (mLuz * objetoX + centroY)) + centroY * (centroY + 2 * mLuz * objetoX) + objetoX^2 * mLuz^2 - lenteRaio^2 + centroX^2
	delta = b^2 - 4 * a * c

	x1 = (-b + Sqr(delta)) / (2 * a)

	x2 = (-b - Sqr(delta)) / (2 * a)

	if (lentePrincipal And (x1 < 0) And (x2 < 0)) Or (((not lentePrincipal) And (x1 > 0) And (x2 > 0))) then
		print x1, x2
	End If

	If (lentePrincipal And x1 >= 0) Or ((Not lentePrincipal) And x1 <= 0) Then
		INCIDE_LENTE_X = x1
	Else 
		If (lentePrincipal And x2 >= 0) Or ((Not lentePrincipal) And x2 <= 0) Then
			INCIDE_LENTE_X = x2
		Else
			INCIDE_LENTE_X = 0
		End If
	End If

End Function

Function HIT_Y(hitX, centroX, centroY, raio, BYVAL positivo As Boolean) As Double

	HIT_Y = Sqr(raio^2 - (hitX-centroX)^2) + centroY

	If Not positivo Then
		HIT_Y = HIT_Y * -1
	End If

End Function

Function NOVO_TETA(teta_luz As Double, centroX As Double, centroY As Double, hitX As Double, hitY As Double, n As Double, BYVAL entrada As Boolean) As Double
	Dim m1 As Double
	Dim m2 As Double
	Dim teta_c As Double
	Dim tg_i As Double
	Dim teta_i As Double
	Dim sin_i As Double
	Dim sin_r As Double
	Dim teta_r As Double

	m1 = tan(teta_luz)
	m2 = (hitY - centroY) / (hitX - centroX)
	teta_c = atn(m2)
	tg_i = abs((m1 - m2) / (1 + m1 * m2))
	teta_i = atn(tg_i)
	sin_i = sin(teta_i)
	If entrada Then
		sin_r = (1 / n) * sin_i
	Else
		sin_r = n * sin_i
	End If
	teta_r = atn(sin_r / Sqr(1 - sin_r^2)) REM gambiarra pra calcular o teta usando o seno e o coseno do angulo

	If (m1 * m2) > 0 Then REM os dois estao no mesmo quadrante
		m1_pos = abs(m1)
		m2_pos = abs(m2)
		If m1_pos > m2_pos Then
			teta_r = teta_r + teta_c
			If teta_c < 0 Then
				NOVO_TETA = teta_r * -1
			ELSE
				NOVO_TETA = teta_r
			End If
		Else
			If teta_c < 0 Then
				NOVO_TETA = teta_r + teta_c
			Else
				NOVO_TETA = teta_c - teta_r
			End If
		End If
	Else
		If teta_c < 0 Then
			NOVO_TETA = teta_r + teta_c
		Else
			NOVO_TETA = teta_c - teta_r
		End If
	End If

End Function

Function INCIDE_PLANO_Y(x As Double, x0 As Double, y0 As Double, teta As Double) As Double
	Dim m As Double

	m = tan(teta)
	INCIDE_PLANO_Y = (x - x0) * m + y0

End Function
